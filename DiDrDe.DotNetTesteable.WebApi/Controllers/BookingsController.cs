﻿using DiDrDe.DotNetTesteable.Application.Contracts;
using DiDrDe.DotNetTesteable.Application.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
// ReSharper disable PossibleInvalidOperationException

namespace DiDrDe.DotNetTesteable.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingsController
        : Controller
    {
        private readonly IBookingCommandService _bookingsCommandService;
        private readonly IBookingQueryService _bookingsQueryService;

        public BookingsController(
            IBookingCommandService bookingsCommandService,
            IBookingQueryService bookingsQueryService)
        {
            _bookingsCommandService = bookingsCommandService;
            _bookingsQueryService = bookingsQueryService;
        }

        [HttpPost]
        public async Task<ActionResult> CreateBooking(Booking booking)
        {
            await _bookingsCommandService.CreateBooking(booking);
            var bookingId = booking.Id.Value;
            var actionName = nameof(GetBooking);
            var routeValues =
                new
                {
                    id = bookingId
                };
            var response = CreatedAtAction(actionName, routeValues, booking);
            return response;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Booking>>> GetBookings()
        {
            var bookings = await _bookingsQueryService.GetBookings();
            var response = Ok(bookings);
            return response;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Booking>> GetBooking(Guid id)
        {
            var booking = await _bookingsQueryService.GetBooking(id);
            var response = Ok(booking);
            return response;
        }
    }
}