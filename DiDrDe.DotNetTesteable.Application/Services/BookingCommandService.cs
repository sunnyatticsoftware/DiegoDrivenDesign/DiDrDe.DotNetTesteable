﻿using DiDrDe.DotNetTesteable.Application.Contracts;
using DiDrDe.DotNetTesteable.Application.Model;
using System.Threading.Tasks;

namespace DiDrDe.DotNetTesteable.Application.Services
{
    public class BookingCommandService
        : IBookingCommandService
    {
        private readonly IDomainBookingFactory _domainBookingFactory;
        private readonly IRepository<Domain.Booking> _bookingRespository;

        public BookingCommandService(
            IDomainBookingFactory domainBookingFactory,
            IRepository<Domain.Booking> bookingRespository)
        {
            _domainBookingFactory = domainBookingFactory;
            _bookingRespository = bookingRespository;
        }

        public async Task CreateBooking(Booking booking)
        {
            var domainBooking = _domainBookingFactory.Create(booking);
            booking.Id = domainBooking.Id;
            await _bookingRespository.Save(domainBooking);
        }
    }
}