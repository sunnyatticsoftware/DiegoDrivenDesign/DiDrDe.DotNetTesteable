﻿using System;

namespace DiDrDe.DotNetTesteable.Application.Model
{
    public class Booking
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
    }
}