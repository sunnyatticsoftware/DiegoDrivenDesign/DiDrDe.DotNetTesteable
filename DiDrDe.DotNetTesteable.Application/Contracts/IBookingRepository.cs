﻿using DiDrDe.DotNetTesteable.Domain.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DiDrDe.DotNetTesteable.Application.Contracts
{
    public interface IRepository<TDomainModel>
        where TDomainModel : IDomainModel
    {
        Task<TDomainModel> GetById(Guid id);
        Task<IEnumerable<TDomainModel>> GetAll();
        Task Save(TDomainModel domainModel);
    }
}