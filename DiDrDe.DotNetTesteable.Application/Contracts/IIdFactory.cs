﻿using System;

namespace DiDrDe.DotNetTesteable.Application.Contracts
{
    public interface IIdFactory
    {
        Guid Create();
    }
}