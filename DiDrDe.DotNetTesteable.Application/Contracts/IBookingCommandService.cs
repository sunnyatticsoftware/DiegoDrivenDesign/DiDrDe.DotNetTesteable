﻿using DiDrDe.DotNetTesteable.Application.Model;
using System.Threading.Tasks;

namespace DiDrDe.DotNetTesteable.Application.Contracts
{
    public interface IBookingCommandService
    {
        Task CreateBooking(Booking booking);
    }
}