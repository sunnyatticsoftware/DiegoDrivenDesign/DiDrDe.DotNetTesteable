﻿using DiDrDe.DotNetTesteable.Domain.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DiDrDe.DotNetTesteable.Application.Contracts
{
    public interface IDatabase<TDatabaseModel>
        where TDatabaseModel : IDomainModel
    {
        Task<TDatabaseModel> Get(Guid id);
        Task<IEnumerable<TDatabaseModel>> GetAll();
        Task Save(TDatabaseModel model);
    }
}