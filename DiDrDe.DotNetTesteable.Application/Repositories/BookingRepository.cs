﻿using DiDrDe.DotNetTesteable.Application.Contracts;
using DiDrDe.DotNetTesteable.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DiDrDe.DotNetTesteable.Application.Repositories
{
    public class BookingRepository
        : IRepository<Booking>
    {
        private readonly IDatabase<Booking> _database;

        public BookingRepository(IDatabase<Booking> database)
        {
            _database = database;
        }

        public async Task<Booking> GetById(Guid id)
        {
            var booking = await _database.Get(id);
            if (booking == null)
            {
                throw new KeyNotFoundException($"Could not retrieve booking  with Id {id}");
            }
            return booking;
        }

        public async Task<IEnumerable<Booking>> GetAll()
        {
            var bookings = await _database.GetAll();
            return bookings;
        }

        public async Task Save(Booking domainModel)
        {
            await _database.Save(domainModel);
        }
    }
}