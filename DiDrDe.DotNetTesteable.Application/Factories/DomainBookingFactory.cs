﻿using DiDrDe.DotNetTesteable.Application.Contracts;
using DiDrDe.DotNetTesteable.Domain;

namespace DiDrDe.DotNetTesteable.Application.Factories
{
    public class DomainBookingFactory
        : IDomainBookingFactory
    {
        private readonly IIdFactory _idFactory;

        public DomainBookingFactory(IIdFactory idFactory)
        {
            _idFactory = idFactory;
        }

        public Booking Create(Model.Booking booking)
        {
            var id = _idFactory.Create();
            var name = booking.Name;
            var domainBooking = new Booking(id, name);
            return domainBooking;
        }
    }
}