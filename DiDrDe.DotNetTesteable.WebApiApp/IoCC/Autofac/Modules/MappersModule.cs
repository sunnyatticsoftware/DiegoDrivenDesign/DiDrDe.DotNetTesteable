﻿using Autofac;
using DiDrDe.DotNetTesteable.Application.Contracts;
using DiDrDe.DotNetTesteable.Application.Mappers;
using DiDrDe.DotNetTesteable.Application.Model;

namespace DiDrDe.DotNetTesteable.WebApiApp.IoCC.Autofac.Modules
{
    public class MappersModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<DomainBookingToBookingMapper>()
                .As<IMapper<Domain.Booking, Booking>>()
                .SingleInstance();
        }
    }
}