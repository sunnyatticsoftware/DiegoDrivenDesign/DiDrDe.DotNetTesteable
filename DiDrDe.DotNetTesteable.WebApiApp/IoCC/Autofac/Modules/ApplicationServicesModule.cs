﻿using Autofac;
using DiDrDe.DotNetTesteable.Application.Contracts;
using DiDrDe.DotNetTesteable.Application.Repositories;
using DiDrDe.DotNetTesteable.Application.Services;
using DiDrDe.DotNetTesteable.Domain;

namespace DiDrDe.DotNetTesteable.WebApiApp.IoCC.Autofac.Modules
{
    public class ApplicationServicesModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<BookingQueryService>()
                .As<IBookingQueryService>();

            builder
                .RegisterType<BookingCommandService>()
                .As<IBookingCommandService>();

            builder
                .RegisterType<BookingRepository>()
                .As<IRepository<Booking>>();
        }
    }
}