﻿using Autofac;
using DiDrDe.Database.Infra.MongoDb.IoCC;
using DiDrDe.Database.Infra.MongoDb.IoCC.Autofac;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace DiDrDe.DotNetTesteable.WebApiApp.IoCC.Autofac.Modules
{
    public class DatabaseModule
        : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterMongoDb(context =>
                {
                    var configuration = context.Resolve<IConfiguration>();
                    const string key = "MongoDb:ConnectionString";
                    var configurationSection = configuration.GetSection(key);
                    if (configurationSection == null)
                    {
                        throw new KeyNotFoundException($"Could not find settings with key {key}");
                    }
                    var connectionString = configurationSection.Value;
                    var mongoDbOptions =
                        new MongoDbOptions
                        {
                            ConnectionString = connectionString
                        };
                    return mongoDbOptions;
                });
        }
    }
}