﻿using DiDrDe.DotNetTesteable.Domain.Contracts;
using System;

namespace DiDrDe.DotNetTesteable.Domain
{
    public class Booking
        : IDomainModel
    {
        public Guid Id { get; }
        public string Name { get; }

        public Booking(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}