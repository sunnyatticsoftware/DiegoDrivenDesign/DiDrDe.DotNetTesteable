﻿using System;

namespace DiDrDe.DotNetTesteable.Domain.Contracts
{
    public interface IDomainModel
    {
        Guid Id { get; }
    }
}