# DiDrDe.DotNetTesteable

## About this project
This is a .NET Core App implemented targeting .NET Core 2.2 framework.
It demonstrates a good project structure following a clean architecture and it uses Autofac and MongoDb driver.
It also demonstrates how to make unit tests and integration tests with xUnit and how to use a TestServer along with Gherkin Specflow to run the integration tests.

## Pre-Requirements
Install .NET Core SDK (>2.2)
Have a mongoDb server up and running