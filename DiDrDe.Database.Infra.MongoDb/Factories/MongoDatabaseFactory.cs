﻿using DiDrDe.Database.Infra.MongoDb.Contracts;
using MongoDB.Driver;

namespace DiDrDe.Database.Infra.MongoDb.Factories
{
    public class MongoDatabaseFactory
        : IMongoDatabaseFactory
    {
        public IMongoDatabase Create(string connectionString)
        {
            var client = new MongoClient(connectionString);
            var mongoUrl = new MongoUrl(connectionString);
            var databaseName = mongoUrl.DatabaseName;
            var database = client.GetDatabase(databaseName);
            return database;
        }
    }
}