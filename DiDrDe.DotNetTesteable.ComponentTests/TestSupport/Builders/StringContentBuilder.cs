﻿using System.Net.Http;
using System.Text;

namespace DiDrDe.DotNetTesteable.ComponentTests.TestSupport.Builders
{
    public class StringContentBuilder
    {
        private string _content;
        private readonly Encoding _encoding;
        private readonly string _mediaType;

        public StringContentBuilder()
        {
            _content = null;
            _encoding = Encoding.UTF8;
            _mediaType = "application/json";
        }

        public StringContentBuilder WithContent(string content)
        {
            _content = content;
            return this;
        }

        public StringContent Build()
        {
            var stringContent = new StringContent(_content, _encoding, _mediaType);
            return stringContent;
        }
    }
}