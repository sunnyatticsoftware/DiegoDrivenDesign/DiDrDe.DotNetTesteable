﻿using DiDrDe.DotNetTesteable.Application.Model;
using Newtonsoft.Json;
using System;

namespace DiDrDe.DotNetTesteable.ComponentTests.TestSupport.Builders
{
    public class BookingBuilder
    {
        private readonly Guid? _id;
        private string _name;

        public BookingBuilder()
        {
            _id = null;
            _name = null;
        }

        public BookingBuilder WithName(string name)
        {
            _name = name;
            return this;
        }

        public Booking Build()
        {
            var booking =
                new Booking
                {
                    Id = _id,
                    Name = _name
                };
            return booking;
        }

        public string BuildAsSerialized()
        {
            var booking = Build();
            var bookingSerialized = JsonConvert.SerializeObject(booking);
            return bookingSerialized;
        }
    }
}