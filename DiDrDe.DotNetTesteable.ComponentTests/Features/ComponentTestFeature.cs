﻿using Autofac.Extensions.DependencyInjection;
using DiDrDe.DotNetTesteable.WebApiApp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using Xunit.Gherkin.Quick;

namespace DiDrDe.DotNetTesteable.ComponentTests.Features
{
    public abstract class ComponentTestFeature
        : Feature
    {
        protected HttpClient HttpClient { get; }

        protected ComponentTestFeature()
        {
            var configuration =
                new ConfigurationBuilder()
                    .AddJsonFile("appsettings.Test.json")
                    .Build();

            var webHostBuilder =
                new WebHostBuilder()
                    .ConfigureServices(services => services.AddAutofac())
                    .UseConfiguration(configuration)
                    .UseStartup<Startup>();

            var server = new TestServer(webHostBuilder);
            HttpClient = server.CreateClient();
        }
    }
}