﻿Feature: SimpleScenario
	As a user
	I want to assign a variable name
	so that I can prove that the BDD approach works

Scenario: Simple Scenario
	Given I choose "foo" as the name
	When I assign the variable
	Then the name should be the assigned value