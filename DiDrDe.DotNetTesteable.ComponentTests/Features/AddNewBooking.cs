﻿using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DiDrDe.DotNetTesteable.ComponentTests.TestSupport.Builders;
using FluentAssertions;
using Xunit.Gherkin.Quick;

namespace DiDrDe.DotNetTesteable.ComponentTests.Features
{
    [FeatureFile("./Features/AddNewBooking.feature")]
    public sealed class AddNewBooking
        : ComponentTestFeature
    {
        private readonly BookingBuilder _bookingBuilder;
        private HttpResponseMessage _result;

        public AddNewBooking()
        {
            _bookingBuilder = new BookingBuilder();
        }

        [Given(@"I choose ""(.*)"" as the booking name")]
        public void I_Choose_The_Booking_Name(string name)
        {
            _bookingBuilder.WithName(name);
        }

        [When(@"I add the booking")]
        public async Task I_Add_The_Booking()
        {
            var bookingSerialized = _bookingBuilder.BuildAsSerialized();
            var stringContent =
                new StringContentBuilder()
                    .WithContent(bookingSerialized)
                    .Build();
            _result = await HttpClient.PostAsync("/api/Bookings", stringContent);
        }

        [Then(@"the booking should be created successfully")]
        public void The_Booking_Should_Be_Created_Successfully()
        {
            _result.StatusCode.Should().Be(HttpStatusCode.Created);
        }

        [And(@"the booking should be accessible through a url using its identifier")]
        public void The_Booking_Should_Have_A_New_Identifier()
        {
            var regularExpression = new Regex("/api/Bookings/(.*)");
            var location = _result.Headers.Location.ToString();
            regularExpression.Match(location).Success.Should().BeTrue();
        }
    }
}