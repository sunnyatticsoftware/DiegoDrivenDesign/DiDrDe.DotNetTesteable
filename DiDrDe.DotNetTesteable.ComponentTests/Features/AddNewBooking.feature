﻿Feature: AddNewBooking
	As a booking system user
	I want to create a new booking
	so that I can make profit

Scenario: Add new booking
	Given I choose "Trip to Bahamas" as the booking name
	When I add the booking
	Then the booking should be created successfully
		And the booking should be accessible through a url using its identifier