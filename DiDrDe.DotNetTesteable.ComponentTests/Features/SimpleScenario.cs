﻿using FluentAssertions;
using Xunit.Gherkin.Quick;

namespace DiDrDe.DotNetTesteable.ComponentTests.Features
{
    [FeatureFile("./Features/SimpleScenario.feature")]
    public sealed class SimpleScenario
        : Feature
    {
        private string _name;

        [Given(@"I choose ""(.*)"" as the name")]
        public void I_Choose_The_Name(string name)
        {
            _name = name;
        }

        [When(@"I assign the variable")]
        public void I_Assign_The_Variable()
        {
            _name = "bar";
        }

        [Then(@"the name should be the assigned value")]
        public void The_Name_Should_Be_The_Assigned_Value()
        {
            _name.Should().Be("bar");
        }
    }
}